#ifndef FIT_H
#define FIT_H

#include <stdio.h>
#include <iomanip>
#include <fstream>
#include <vector>
#include <array>
#include <iostream>
#include <sstream>

#include <TCanvas.h>
#include <TH1.h>
#include <TAxis.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMinuit.h>

#include "FitModels.h"
#include "WriteXml.h"

using namespace std;

/////////////////////////////////////////////////
// Returns PMT number given its position on EC //
/////////////////////////////////////////////////
int idxPMT(const TString &PMTpos) {
  int idx = -1;
  if (PMTpos == "A") idx = 0;
  if (PMTpos == "B") idx = 1;
  if (PMTpos == "C") idx = 3;
  if (PMTpos == "D") idx = 2;
  return idx;
}


//////////////////////////////////////////////////
// Returns EC number given EC and PMT positions //
//////////////////////////////////////////////////
int idxEC(const TString &ECpos, const TString &PMTpos) {
  const int nbEC = 4;
  const int iPMT = idxPMT(PMTpos);

  int idx = -1; // convert to index
  if (ECpos == "TT") idx = 0; 
  if (ECpos == "MT") idx = 1; 
  if (ECpos == "MB") idx = 2; 
  if (ECpos == "BB") idx = 3; 

  if (iPMT == 2 || iPMT == 3) idx = (nbEC -1) - idx;
  return idx;
}


////////////////////////// // 
// Returns charge (Me-)
////////////////////////// 
float charge(const float G_MAROC, const float G_ADC) { 
  float c = -8.758;
  float d = 1.302;
  float e = -0.0014;
  float f = -0.565; 
  float g = 0.013;
  float h = 0.0002; 
  if (G_MAROC == 0. || G_ADC == 0.) return 0.; 
  float charge = G_ADC / (c + d*G_MAROC + e*pow(G_MAROC, 2)) - ((f + g*G_MAROC + h*pow(G_MAROC, 2)) / (c + d*G_MAROC + e*pow(G_MAROC, 2))); // Charge in Me- 
  charge *= 10*1e3 / 1.6; // 1e-12 (pF) / 1.6e-19 (Qe-) 
  return charge; }

////////////////////////// // 
// Returns charge (Me-)
////////////////////////// 
float conversion(const float var_ADC,const float G_MAROC,const float G_ADC)
{ 
  float conversion =  var_ADC  *  charge(G_MAROC, G_ADC) /G_ADC;
  return conversion; }

///////////////////
// Fit PMT of EC //
///////////////////
void fit(const TString &filename, 
         const int min_step_th, 
         const int max_step_th, 
         const int offset, 
         const int attenuation, 
         const TString &ECpos, 
         TString PMTpos, // hack for EC-H!!! should be const!!!
         const TString &descr) { 

  // Constants
  const double min_mu = 0.05; // min. estimated occupancy required to perform the fit
  const double min_mu_ct = 0.01; // if less, cross-talk is set to zero
  const double min_efficiency = 0.95; // min. efficiency required
  const double calib[4] = {25., 45., 85., 240.}; // calib at different attenuations (ke)

  // Sanity checks
  if (offset != 0 && offset != 1) { 
    cout << "[ERROR] Wrong value of the offset = " << offset << endl;
    exit(0);
  }
  if (attenuation != 0 && attenuation != 1 && attenuation != 2 && attenuation != 3) { 
    cout << "[ERROR] Wrong value of the attuenation = " << attenuation << endl;
    exit(0);
  }

  // Print parameters
  cout << "---------------" << endl;
  cout << "FIT PARAMETERS:" << endl;
  cout << "---------------" << endl;
  cout << "Input file: " << filename << endl;
  cout << "Offset: " << offset << endl;
  cout << "Attenuation: " << attenuation << endl;
  cout << "Threshold scan range: [" << min_step_th << ", " << max_step_th << "]" << endl;
  cout << "EC: " << ECpos << ", PMT: " << PMTpos << endl; 
  cout << endl;

  // ROOT batch mode
  //gROOT->SetBatch();
  gStyle->SetOptFit(1111);

  // Read calibration files
  TFile *calib_file = new TFile("/eos/lhcb/testbeam/rich/tb2017Oct/data/calibration.root");
  TTree *cal = (TTree*)calib_file->Get("calibration"); 
  double thresholdstep_att0, thresholdstep_att1, thresholdstep_att2;
  double offset_att0, offset_att1, offset_att2;  
  int EC, PMT, CH;
  double calstep[4][4][64];
  double offstep[4][4][64];

  cal->SetBranchAddress("EC",&EC);
  cal->SetBranchAddress("PMT",&PMT);
  cal->SetBranchAddress("CH",&CH);
  cal->SetBranchAddress("thresholdstep_att0",&thresholdstep_att0);
  cal->SetBranchAddress("thresholdstep_att1",&thresholdstep_att1); 
  cal->SetBranchAddress("thresholdstep_att2",&thresholdstep_att2);
  cal->SetBranchAddress("offset_att0",&offset_att0);
  cal->SetBranchAddress("offset_att1",&offset_att1);
  cal->SetBranchAddress("offset_att2",&offset_att2);

  for(int g = 0; g < cal->GetEntries(); g++) {
    cal->GetEntry(g);
    double thresholdstep_att_v[3] = { thresholdstep_att0, thresholdstep_att1, thresholdstep_att2 };
    double offset_att_v[3] = {offset_att0, offset_att1, offset_att2 };
    for (int m = 0;  m <= 2; m++) {
      //  thresholdstep_att_v[m] = calib[m]; // average calib
      // offset_att_v[m] = 0.;
        if (thresholdstep_att_v[m] == -1) { // if calib not available, use nominal
          thresholdstep_att_v[m] = calib[m];
           offset_att_v[m] = 0.;
         }
      if (attenuation == m)  {
      calstep[EC][PMT][CH-1] = thresholdstep_att_v[m];
      offstep[EC][PMT][CH-1] = offset_att_v[m];
       }
      
    }
  }
  calib_file->Close();


   // Read Maroc file (Silvia)
    float  mean_1ph[64];
    float  mean_1ct[64];
    float  sigma_1ct[64];
    float Pmiss[64];
    //int  maroc_gain_ADC = 156;   TFile *maroc_file = new TFile("/eos/lhcb/user/s/sgambett/PDQA/R13742/preseries/3012/fit-feb-92393570-003012-pmt2.root"); // FA0302 900V TT
     int  maroc_gain_ADC = 128;    TFile *maroc_file = new TFile("/eos/lhcb/user/s/sgambett/PDQA/R13742/preseries/260/fit-feb-C464BF09-000260-pmt2.root"); //FA0216 900V MT

    //    int  maroc_gain_ADC = 128;    TFile *maroc_file = new TFile("/eos/lhcb/user/s/sgambett/PDQA/R13742/preseries/3012/fit-feb-92393570-003012-pmt1.root");

    TTree *mar = (TTree*)maroc_file->Get("tree");
    mar->SetBranchAddress("mean_1ph",&mean_1ph);
    mar->SetBranchAddress("mean_1ct",&mean_1ct);
    mar->SetBranchAddress("sigma_1ct",&sigma_1ct);
    mar->SetBranchAddress("Pmiss",&Pmiss);
    mar->GetEntry(0);
    maroc_file->Close();

  // Open input/output files + tree 
  TFile *input_file = TFile::Open(filename);
  TString tag = ((TObjString*)filename.Tokenize("/")->Last())->GetString();
  tag.ReplaceAll(".root", "");
  TFile *output_file = new TFile( Form("fits_%s_%s_%s.root", tag.Data(), ECpos.Data(), PMTpos.Data()), "RECREATE");

  // Set tree
  int fit_status_sig, fit_status;
  int th_wofit;
  double chi2_fit, ped_fit, gain_fit;
  double mu_fit, sigma_ped_fit, sigma_sig_fit, Pmiss_fit;
  double mu_ct_fit, mean_ct_fit, sigma_ct_fit;
  double th_min, th_eff;
  float  Gain_Maroc;
  int npixel;
  
   double par_gain;
   double par_sigma_sig;
   
   double step;
   

  TTree *tree = new TTree("tree", "tree");
  tree->Branch("chi2_fit", &chi2_fit);
  tree->Branch("ped_fit", &ped_fit);
  tree->Branch("sigma_ped_fit", &sigma_ped_fit);
  tree->Branch("sigma_sig_fit", &sigma_sig_fit);
  tree->Branch("par_sigma_sig",&par_sigma_sig);
  tree->Branch("gain_fit", &gain_fit);
  tree->Branch("par_gain", &par_gain);
  tree->Branch("mu_fit", &mu_fit);
  tree->Branch("mu_ct_fit", &mu_ct_fit);
  tree->Branch("mean_ct_fit", &mean_ct_fit);
  tree->Branch("sigma_ct_fit", &sigma_ct_fit);
  tree->Branch("Pmiss_fit", &Pmiss_fit);
  tree->Branch("fit_status", &fit_status);
  tree->Branch("fit_status_sig", &fit_status_sig);
  tree->Branch("th_min", &th_min);
  tree->Branch("th_eff", &th_eff);
  tree->Branch("th_wofit", &th_wofit);
  tree->Branch("Gain_Maroc", &Gain_Maroc);
  tree->Branch("npixel", &npixel);

  // Book histograms
  TH1F *hs[NPX];
  TH1F *h[NPX];
  TH1F *hder[NPX];
  TF1 *datader[NPX];
  TGraph *totder[NPX], *sigder[NPX], *ctder[NPX];
  TF1 *totpdf[NPX], *sigpdf[NPX], *noisepdf[NPX], *ctpdf[NPX];
  TH1D *histochi2 = new TH1D("histochi2", "histochi2", 100, 0,0);
  TH1D *histosigmapedestal = new TH1D("histosigmapedestal", "sigma_pedestal", 100, 0.0, 0.005);
  TH1D *histosigma = new TH1D("histosigma", "sigma signal", 100, 0.0, 20.0);
  TH1D* hthresholds_eff = new TH1D("hthresholds_eff", "Optimized thresholds eff", 63, 0, 63);
  TH1D* hthresholds_min = new TH1D("hthresholds_min", "Optimized thresholds minimum", 63, 0, 63);
  TH1D* hthresholds_wof = new TH1D("hthresholds_wof", "Optimized thresholds w/o fit", 63, 0, 63);
  TH1D* hsob = new TH1D("hsob", "Signal over background for optimized threshold", 50, -1, 10);
  TH1D *histopedestal;
  if (offset == 1) histopedestal = new TH1D("histopedestal", "pedestal", 30,20.0,50.0);
  else histopedestal = new TH1D("histopedestal", "pedestal", 30,-30.0,20.0);
  TH1D *histopeak = new TH1D("histopeak", "singlephotonpeak", int(max_step_th - min_step_th), min_step_th, max_step_th);
  TH1D *histomu = new TH1D("histomu", "mu", 100, 0.0, 0.1);  




  /////////////////////
  // Fit each pixels //
  /////////////////////
  for(int i = 0; i < NPX; i++) {
    cout << "[INFO] ------------------" << endl;
    cout << "[INFO] Fitting channel: " << i + 1 << endl;
    cout << "[INFO] ------------------" << endl;
    npixel =i;
    
    // from ADC to Me-
     Gain_Maroc= charge(maroc_gain_ADC, mean_1ph[i] ); 

    cout<<"::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: "<<mean_1ph[i]<<"  "<<Gain_Maroc<<endl;
    

    // Get threshold scan histogram
    const int iEC = idxEC(ECpos, PMTpos); // DB numbering scheme 
    const int iPMT = idxPMT(PMTpos);
    hs[i] = (TH1F*)input_file->Get( Form("S-Curve EC%i PMT%i Ch %i", iEC, iPMT, i + 1) );
    hs[i]->SetName( Form("histogram%i", i + 1) );
    hs[i]->SetTitle( Form("histogram%i", i + 1) );

    //from threshold step to threshold value (ke)
    const int nbins = max_step_th - min_step_th;
    step = calstep[iEC][iPMT][i]; // step in ke
    const double min_step = min_step_th*step + offstep[iEC][iPMT][i];  
    const double max_step = max_step_th*step + offstep[iEC][iPMT][i];
    h[i] = new TH1F(Form("h%i", i + 1),"",nbins, min_step, max_step);

    for (int st = 1; st <= nbins; st++) {  
      h[i]->SetBinContent(st, hs[i]->GetBinContent(min_step_th + st));    
      h[i]->SetBinError(st, hs[i]->GetBinError(min_step_th + st));    
    }

    // Initial estimation of fit parameters
    cout << "[INFO] Initial parameter estimation..." << endl;

    // pedestal
    double par_ped = 0.;
    for(int j = 1; j <= nbins; j++) {
      if (h[i]->GetBinContent(j) > 0.8) 
        par_ped = h[i]->GetXaxis()->GetBinCenter(j);
    }
    const int bin_ped = h[i]->FindBin(par_ped); 

    // occupancy 
    double par_mu = h[i]->GetBinContent(bin_ped + 2);
    const bool do_fit = (par_mu > min_mu);

    // gain and sigma signal
    const double sig_thr = par_ped + step;
    TH1F* hsmooth = (TH1F*)h[i]->Clone(Form("hsmooth%i", i + 1));
    hsmooth->GetXaxis()->SetRangeUser(sig_thr, max_step); // smooth only signal and no pedestal
      hsmooth->Smooth(1, "R");
    TF1* hfun = new TF1(Form("hfun%i", i + 1), 
	[&](double *x, double *p) { return hsmooth->Interpolate(x[0]); }, min_step, max_step, 0); 

    datader[i] = new TF1(Form("derivative_data%i", i + 1), // from ped + th. step
	[&](double *x, double *p) { return (x[0] >= sig_thr ? -hfun->Derivative(x[0]) : 0.); }, min_step, max_step, 0); 

    hder[i] = (TH1F*)datader[i]->GetHistogram()->Clone( Form("hder%i", i + 1));
    hder[i]->SetBinContent(hder[i]->FindBin(sig_thr), 0.); // fix spike ROOT problem at the edge of the histo...

    // estimation of signal initial parameters 
    auto fun_sig = new TF1(Form("fun_sig%i", i + 1), "gaus", min_step, max_step);
    hder[i]->Fit(fun_sig, "R", "G", 1.3*par_ped, max_step);
    par_gain = fun_sig->GetParameter("Mean") - par_ped;
    par_sigma_sig = fun_sig->GetParameter("Sigma");
    double sig_int = fun_sig->Integral(1.3*par_ped, max_step);
    cout<<"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"<<endl;
    
    cout<<step<<" "<<i<<endl;
    

    // estimation of cross talk initial parameters 
    auto fun_ct = new TF1(Form("fun_ct%i", i + 1), "gaus", min_step, max_step);
    hder[i]->Fit(fun_ct, "R+", "G", par_ped, 1.1*par_ped);
    double par_mean_ct = fun_ct->GetParameter("Mean") - par_ped;
    double par_sigma_ct = fun_ct->GetParameter("Sigma");
    double ct_int = fun_ct->Integral(par_ped, 1.1*par_ped);
    double par_mu_ct = par_mu * ct_int / sig_int;

    //if (par_mu_ct < min_mu_ct) par_mu_ct = 0.;
    //par_mu_ct = par_mean_ct = par_sigma_ct = 0.;

    // fix params to what found to MAROC data
    float par_Pmiss = Pmiss[i];

    par_mean_ct = conversion(mean_1ct[i],  maroc_gain_ADC, mean_1ph[i] );
    par_sigma_ct = conversion(sigma_1ct[i],  maroc_gain_ADC, mean_1ph[i] );
    // cout<< par_Pmiss<<" "<<par_mean_ct<<" "<<par_sigma_ct<<endl;
    // cout<< Pmiss[i]<<" "<<mean_1ct[i]<<" "<<sigma_1ct[i]<<endl; 
 
 
     
    // Fitting range
    const double min_fit = min_step; //par_ped + step; 
    const double max_fit = max_step;

    // Model pdf
    totpdf[i] = new TF1(Form("totpdf%i", i + 1), tot_pdf, min_step, max_step, 9);
    totpdf[i]->SetLineColor(kBlue);
    totpdf[i]->SetNpx(1000);

    // Set fitting parameters
    totpdf[i]->SetParName(0, "mu");
    totpdf[i]->SetParameter(0, par_mu); 
    totpdf[i]->SetParLimits(0, 0.5*par_mu, 5*par_mu);

    totpdf[i]->SetParName(1, "mu_ct");
    totpdf[i]->SetParameter(1, par_mu_ct); 
    totpdf[i]->SetParLimits(1, 0., 7.*par_mu_ct);
    //totpdf[i]->FixParameter(1, par_mu_ct); // fix

    totpdf[i]->SetParName(2, "pedestal");
    totpdf[i]->SetParameter(2, par_ped); 
    totpdf[i]->SetParLimits(2, 0.7*par_ped, 1.3*par_ped);
    totpdf[i]->FixParameter(2, par_ped); // fix

    totpdf[i]->SetParName(3, "sigma pedestal");
    totpdf[i]->FixParameter(3, 1e-6); // fix

    totpdf[i]->SetParName(4, "gain");
    totpdf[i]->SetParameter(4, par_gain);
    totpdf[i]->SetParLimits(4, 0.5*par_gain, 1.5*par_gain); 

    totpdf[i]->SetParName(5, "sigma_sig");
    totpdf[i]->SetParameter(5, par_sigma_sig);
    totpdf[i]->SetParLimits(5, 0.5*par_sigma_sig, 5*par_sigma_sig);

    totpdf[i]->SetParName(6, "mean_ct");
    totpdf[i]->SetParameter(6, par_mean_ct);
    totpdf[i]->SetParLimits(6, 0., 2.*par_mean_ct);
    totpdf[i]->FixParameter(6, par_mean_ct); // fix

    totpdf[i]->SetParName(7, "sigma_ct");
    totpdf[i]->SetParameter(7, par_sigma_ct);
    totpdf[i]->SetParLimits(7, 0., 2.*par_sigma_ct);
    totpdf[i]->FixParameter(7, par_sigma_ct); // fix

    totpdf[i]->SetParName(8, "Pmissed");
    totpdf[i]->SetParameter(8, par_Pmiss);
    totpdf[i]->SetParLimits(8, 0.5*par_Pmiss, 1.5*par_Pmiss);
    totpdf[i]->FixParameter(8, par_Pmiss); // fix

    // noise pdf
    noisepdf[i] = new TF1( Form("noisepdf%i", i + 1), 
	[&](double *x, double *p) { return signal_crosstalk_pdf(x, 0, 0, p); }, min_step, max_step, 9 );
    noisepdf[i]->SetLineColor( kBlack );
    noisepdf[i]->SetNpx(1000);

    // signal pdf
    sigpdf[i] = new TF1( Form("sigpdf%i", i + 1), 
	[&](double *x, double *p) { return signal_crosstalk_pdf(x, 1, 0, p); }, min_step, max_step, 9 ); 
    sigpdf[i]->SetLineColor( kRed );
    sigpdf[i]->SetNpx(1000);   

    // cross-talk pdf
    ctpdf[i] = new TF1( Form("ctpdf%i", i + 1), 
	[&](double *x, double *p) { return signal_crosstalk_pdf(x, 0, 1, p); }, min_step, max_step, 9 );
    ctpdf[i]->SetLineColor( kViolet );
    ctpdf[i]->SetNpx(1000);

    // Do the full fit
    if (do_fit) {
      cout << endl;
      cout << "[INFO] ------------------" << endl;
      cout << "[INFO] Fitting range: [" << min_fit << ", " << max_fit <<"]" << endl;
      cout << "[INFO] Initial parameters:" << endl;
      cout << "[INFO] - mu: " << par_mu << endl;
      cout << "[INFO] - pedestal (ke-): " << par_ped << endl;
      cout << "[INFO] - gain (ke-): " << par_gain << endl;
      cout << "[INFO] - sigma sig (ke-): " << par_sigma_sig << endl;
      cout << "[INFO] - mu cross-talk: " << par_mu_ct << endl;
      cout << "[INFO] - mean cross-talk (ke-): " << par_mean_ct << endl;
      cout << "[INFO] - sigma cross-talk (ke-): " << par_sigma_ct << endl;
      cout << "[INFO] - Pmiss: " << par_Pmiss << endl;
      cout << "[INFO] ------------------" << endl;

      // Fit!
      fit_status = h[i]->Fit(totpdf[i], "R", "G" , min_fit, max_fit);
      chi2_fit = totpdf[i]->GetChisquare() / totpdf[i]->GetNDF(); 
      histochi2->Fill(chi2_fit);

      cout << "[INFO] Fit status: " << gMinuit->fCstatu.Data() << endl;
      cout << endl;

      // Get final pdfs & fit parameters
      mu_fit = totpdf[i]->GetParameter("mu");
      mu_ct_fit = totpdf[i]->GetParameter("mu_ct");
      gain_fit = totpdf[i]->GetParameter("gain");
      sigma_sig_fit = totpdf[i]->GetParameter("sigma_sig");
      ped_fit = totpdf[i]->GetParameter("pedestal");
      sigma_ped_fit = totpdf[i]->GetParameter("sigma pedestal");
      mean_ct_fit = totpdf[i]->GetParameter("mean_ct");
      sigma_ct_fit = totpdf[i]->GetParameter("sigma_ct");
      Pmiss_fit = totpdf[i]->GetParameter("Pmissed");
    
      noisepdf[i]->SetParameters(
	  mu_fit, mu_ct_fit, ped_fit, sigma_ped_fit, gain_fit, sigma_sig_fit, mean_ct_fit, sigma_ct_fit, Pmiss_fit);
      sigpdf[i]->SetParameters(
	  mu_fit, mu_ct_fit, ped_fit, sigma_ped_fit, gain_fit, sigma_sig_fit, mean_ct_fit, sigma_ct_fit, Pmiss_fit);
      ctpdf[i]->SetParameters(
	  mu_fit, mu_ct_fit, ped_fit, sigma_ped_fit, gain_fit, sigma_sig_fit, mean_ct_fit, sigma_ct_fit, Pmiss_fit);
    }

    else {
      cout << "[INFO]"<<endl;
      cout << "[INFO] ****************************" << endl;
      cout << "[INFO] Estimated occupancy " << par_mu << " too low. Cannot fit!" << endl;
      cout << "[INFO] ****************************" << endl;
      cout << "[INFO]"<<endl;
  
      mu_fit = mu_ct_fit = -1; 
      gain_fit = ped_fit = mean_ct_fit = -1;
      sigma_sig_fit = sigma_ct_fit = sigma_ped_fit = -1;
      Pmiss_fit = -1; 

      noisepdf[i]->SetParameters(1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4);
      sigpdf[i]->SetParameters(1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4);
      ctpdf[i]->SetParameters(1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4);

    }

    // Do the derivative 
    totder[i] = new TGraph(); // total
    totder[i]->SetName( Form("derivative%i", i + 1) );
    totder[i]->SetTitle( Form("derivative%i", i + 1) );
    totder[i]->SetLineColor( kBlack );

    sigder[i] = new TGraph(); // signal 
    sigder[i]->SetName( Form("derivative_sig%i", i + 1) );
    sigder[i]->SetTitle( Form("derivative_sig%i", i + 1) );
    sigder[i]->SetLineColor( kRed );

    ctder[i] = new TGraph(); // cross-talk
    ctder[i]->SetName( Form("derivative_ct%i", i + 1) );
    ctder[i]->SetTitle( Form("derivative_ct%i", i + 1) );
    ctder[i]->SetLineColor( kViolet );

    const double dx = 10.; //5e-1; // fill graph w/ derivative of fit function
    double xj = min_step;
    while (xj <= max_step) {
      int j = (xj - min_step) / dx;
      totder[i]->SetPoint(j, xj, (do_fit ? -totpdf[i]->Derivative(xj) : 0.) );
      sigder[i]->SetPoint(j, xj, (do_fit ? -sigpdf[i]->Derivative(xj) : 0.) );
      ctder[i]->SetPoint(j, xj, (do_fit ? -ctpdf[i]->Derivative(xj) : 0.) );
      xj += dx;
    }

    // Find optimized threshold based on efficiency
    th_eff = min_step;
    double sig_max = sigpdf[i]->GetMaximum();
    double sig_eff = 1.; 
    while (sig_eff >= min_efficiency && th_eff <= max_step) {
      sig_eff = sigpdf[i]->Eval(th_eff++) / sig_max;
    }

    // Find threshold without fit
    for(int j = max_step_th; j > min_step_th; j--) {
      if (h[i]->GetBinContent(j) > 0.8) {
        th_wofit =  h[i]->GetXaxis()->GetBinCenter(j+5);
        break;
      }
    }

    // Fill histograms and tree
    histopedestal->Fill(ped_fit);
    histopeak->Fill(gain_fit);
    histomu->Fill(mu_fit);
    histosigmapedestal->Fill(sigma_ped_fit);
    histosigma->Fill(sigma_sig_fit);
    datader[i]->Write();
    tree->Fill();
  }//ipx


  ///////////////////////////////
  // Write thresholds XML file // 
  ///////////////////////////////
  //writeXML( "95_eff_" + descr, ECpos, PMTpos, thresholds_eff, offset, attenuation );
  //writeXML( "min_" + descr, ECpos, PMTpos, thresholds_min, offset, attenuation );
  //writeXML( "wofit_" + descr, ECpos, PMTpos, thresholds_wofit, offset, attenuation );


  ////////////////
  // Save plots //
  ////////////////
  TCanvas *chist = new TCanvas("chist", "S-curve", 1000, 1000);
  chist->Divide(8, 8);

  TCanvas *cder = new TCanvas("cder", "S-curve derivative", 1000, 1000);
  cder->Divide(8, 8);

  for (int i = 0; i < NPX; i++) { 
    TVirtualPad* pad = chist->cd(i + 1); 
    pad->SetLogy();
    h[i]->Draw(); 
    noisepdf[i]->Draw("same");
    sigpdf[i]->Draw("same");

    pad = cder->cd(i + 1); 
    pad->SetLogy();
    //float ymax = sigder[i]->GetHistogram()->GetMaximum();
    //sigder[i]->GetYaxis()->SetRangeUser(0, 1.2*ymax);
    //sigder[i]->GetYaxis()->SetRangeUser(1e-4, 1e-3);
    sigder[i]->Draw(); 
  }

  for (int i = 0; i < NPX; i++) {
    h[i]->Write();
    hder[i]->Write();
    totpdf[i]->Write();
    noisepdf[i]->Write();
    sigpdf[i]->Write();
    ctpdf[i]->Write();
    totder[i]->Write();
    sigder[i]->Write();
    ctder[i]->Write();
  }

  histochi2->Write();
  histopedestal->Write();
  histosigmapedestal->Write();
  histopeak->Write();
  histomu->Write();
  histosigma->Write();
  hthresholds_eff->Write();
  hthresholds_min->Write();
  hthresholds_wof->Write();
  hsob->Write();
  chist->Write();
  cder->Write();
  output_file->Write();
  output_file->Close();
}
#endif
