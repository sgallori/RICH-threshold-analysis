#ifndef WRITEXML_H
#define WRITEXML_H

#include <sstream>
#include <iomanip>
#include <TMath.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>

using namespace std;

#define NPX 64 // number of PMT pixels

void writeXML(const TString &description, 
              const TString &ECpos, 
              TString PMTpos, // hack for EC-H!! should be const!!!
              const int *thresholds,
              const int offset,
              const int attenuation) {
  
  // Hack for EC-H!!!
  if (ECpos == "MB" || ECpos =="BB") {
   if (PMTpos == "A") PMTpos = "H";
   else return;  
  }

  // Create new XML file
  ofstream file; // prevent to append the same file again...
  if (PMTpos == "A" || PMTpos == "H") file.open(description + "_" + ECpos + ".xml"); 
  else file.open(description + "_" + ECpos + ".xml", ios_base::app);

  // Write common block 
  if (PMTpos == "A" || PMTpos == "H") {
    file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << endl;
    file << "<!DOCTYPE preferences SYSTEM \"http://java.sun.com/dtd/preferences.dtd\">" << endl;
    file << "<preferences EXTERNAL_XML_VERSION=\"1.0\">" << endl;
    file << "  <root type=\"user\">" << endl;
    file << "    <map/>" << endl;
    file << "    <node name=\"RICH\">" << endl;
    file << "      <map/>" << endl;
    file << "      <node name=\"preferences\">" << endl;
    file << "        <map/>" << endl;
    file << "        <node name=\"" << ECpos + "_" + description << "\">" << endl; 
    file << "          <map/>" << endl;
  }

  // Write common block for each PMT
  file << "          <node name=\"" << PMTpos << "\">" << endl;
  file << "            <map>" << endl;
  file << "              <entry key=\"claro.SEU-enb\" value=\"false\"/>" << endl;
  file << "              <entry key=\"claro.SEU-reset\" value=\"false\"/>" << endl;
  file << "              <entry key=\"claro.TP-enb\" value=\"false\"/>" << endl;
  file << "              <entry key=\"claro.aux-TP\" value=\"false\"/>" << endl;
  file << "              <entry key=\"claro.intern-corr\" value=\"false\"/>" << endl;
  file << "              <entry key=\"claro.mux\" value=\"0\"/>" << endl;
  file << "            </map>" << endl;

  // Loop over CLARO channels
  for (int i = 1; i <= NPX; i++) {
    // Write thresholds
    if (i < 10) file << "            <node name=\"0" << i << "\">" << endl; 
    else file << "            <node name=\"" << i << "\">" << endl;
    file << "              <map>" << endl;
    file << "                <entry key=\"claro.attenuation\" value=\"" << attenuation << "\"/>" << endl;
    file << "                <entry key=\"claro.hysteresis-enable\" value=\"true\"/>" << endl;
    file << "                <entry key=\"claro.input-enable\" value=\"true\"/>" << endl;
    file << "                <entry key=\"claro.testpulse-disable\" value=\"true\"/>" << endl;
    file << "                <entry key=\"claro.threshold\" value=\"" << thresholds[i-1] << "\"/>" << endl; 
    file << "                <entry key=\"claro.threshold-offset\" value=\"" << (offset == 0 ? "false" : "true") << "\"/>" << endl;
    file << "              </map>" << endl;
    file << "            </node>" << endl;
  }//ch
  file << "          </node>" << endl;

  // Write last common block at the end
  if (PMTpos == "D" || PMTpos == "H") {
    file << "        </node>"<<endl;
    file << "      </node>"<<endl;
    file << "    </node>"<<endl;
    file << "  </root>"<<endl;
    file << "</preferences>"<<endl;
  }

  // Close XML file
  file.close();
}
#endif
