#ifndef FIT_H
#define FIT_H

#include <stdio.h>
#include <iomanip>
#include <fstream>
#include <vector>
#include <array>
#include <iostream>
#include <sstream>

#include <TCanvas.h>
#include <TH1.h>
#include <TAxis.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMinuit.h>

#include "FitModels.h"
#include "WriteXml.h"

using namespace std;

/////////////////////////////////////////////////
// Returns PMT number given its position on EC //
/////////////////////////////////////////////////
int idxPMT(const TString &PMTpos) {
  int idx = -1;
  if (PMTpos == "A") idx = 0;
  if (PMTpos == "B") idx = 1;
  if (PMTpos == "C") idx = 3;
  if (PMTpos == "D") idx = 2;
  return idx;
}


//////////////////////////////////////////////////
// Returns EC number given EC and PMT positions //
//////////////////////////////////////////////////
int idxEC(const TString &ECpos, const TString &PMTpos) {
  const int nbEC = 4;
  const int iPMT = idxPMT(PMTpos);

  int idx = -1; // convert to index
  if (ECpos == "TT") idx = 0; 
  if (ECpos == "MT") idx = 1; 
  if (ECpos == "MB") idx = 2; 
  if (ECpos == "BB") idx = 3; 

if (iPMT == 2 || iPMT == 3) idx = (nbEC -1) - idx;
  return idx;
}


///////////////////
// Fit PMT of EC //
///////////////////
void fit(const TString &filename, 
         const int min_step_th, 
         const int max_step_th, 
         const int offset, 
         const int attenuation, 
         const TString &ECpos, 
         TString PMTpos, // hack for EC-H!!! should be const!!!
         const TString &descr) { 

  // Constants
  const double calib[4] = {25., 45., 85., 240.}; // calib at different attenuations (ke)
  //const int offset_steps = 32;          // offset value in thresholds steps
  const double min_efficiency = 0.95;   // Mimimum efficiency required

  int thresholds_eff[NPX] = { 0 };
  int thresholds_min[NPX] = { 0 };
  int thresholds_wofit[NPX] = { 0 };


  // Sanity checks
  if (offset != 0 && offset != 1) { 
    cout << "[ERROR] Wrong value of the offset = " << offset << endl;
    exit(0);
  }
  if (attenuation != 0 && attenuation != 1 && attenuation != 2 && attenuation != 3) { 
    cout << "[ERROR] Wrong value of the attuenation = " << attenuation << endl;
    exit(0);
  }

  // Print parameters
  cout << "---------------" << endl;
  cout << "FIT PARAMETERS:" << endl;
  cout << "---------------" << endl;
  cout << "Input file: " << filename << endl;
  cout << "Offset: " << offset << endl;
  cout << "Attenuation: " << attenuation << endl;
  cout << "Threshold scan range: [" << min_step_th << ", " << max_step_th << "]" << endl;
  cout << "EC: " << ECpos << ", PMT: " << PMTpos << endl; 
  cout << endl;

  // ROOT batch mode
  //gROOT->SetBatch();

  // Open input/output files + tree 
  TFile *input_file = TFile::Open(filename);
  TString tag = ((TObjString*)filename.Tokenize("/")->Last())->GetString().ReplaceAll(".root", "");


  // Set tree
  int fit_status_sig, fit_status;
  int th_wofit;
  double chi2_fit, ped_fit, gain_fit;
  double mu_fit, sigma_ped_fit, sigma_sig_fit, Pmiss_fit;
  double mu_ct_fit, mean_ct_fit, sigma_ct_fit;
  double th_min, th_eff;
  double gainMe_fit;

  TTree *tree = new TTree("tree", "tree");
  tree->Branch("chi2_fit", &chi2_fit);
  tree->Branch("ped_fit", &ped_fit);
  tree->Branch("sigma_ped_fit", &sigma_ped_fit);
  tree->Branch("sigma_sig_fit", &sigma_sig_fit);
  tree->Branch("gain_fit", &gain_fit);
  tree->Branch("mu_fit", &mu_fit);
  tree->Branch("mu_ct_fit", &mu_ct_fit);
  tree->Branch("mean_ct_fit", &mean_ct_fit);
  tree->Branch("sigma_ct_fit", &sigma_ct_fit);
  tree->Branch("Pmiss_fit", &Pmiss_fit);
  tree->Branch("fit_status", &fit_status);
  tree->Branch("fit_status_sig", &fit_status_sig);
  tree->Branch("th_min", &th_min);
  tree->Branch("th_eff", &th_eff);
  tree->Branch("th_wofit", &th_wofit);
  tree->Branch("gainMe_fit", &gainMe_fit);

  // Book histograms
  TH1F *hs[NPX];
  // TGraphErrors *h[NPX];
  TH1F *h[NPX];
  TF1 *hder[NPX];
  TGraph *totder[NPX], *sigder[NPX], *ctder[NPX];
  TF1 *totpdf[NPX], *sigpdf[NPX], *noisepdf[NPX], *ctpdf[NPX];
  TH1D *histochi2 = new TH1D("histochi2", "histochi2", 100, 0,0);
  TH1D *histosigmapedestal = new TH1D("histosigmapedestal", "sigma_pedestal", 100, 0.0, 0.005);
  TH1D *histosigma = new TH1D("histosigma", "sigma signal", 100, 0.0, 20.0);
  TH1D* hthresholds_eff = new TH1D("hthresholds_eff", "Optimized thresholds eff", 63, 0, 63);
  TH1D* hthresholds_min = new TH1D("hthresholds_min", "Optimized thresholds minimum", 63, 0, 63);
  TH1D* hthresholds_wof = new TH1D("hthresholds_wof", "Optimized thresholds w/o fit", 63, 0, 63);
  TH1D* hsob = new TH1D("hsob", "Signal over background for optimized threshold", 50, -1, 10);
  TH1D *histopedestal;
  if (offset == 1) histopedestal = new TH1D("histopedestal", "pedestal", 30,20.0,50.0);
  else histopedestal = new TH1D("histopedestal", "pedestal", 30,-30.0,20.0);
  TH1D *histopeak = new TH1D("histopeak", "singlephotonpeak", int(max_step_th - min_step_th), min_step_th, max_step_th);
  TH1D *histomu = new TH1D("histomu", "mu", 100, 0.0, 0.1);
 
  //anna
  //calibration
  TFile *f = new TFile("/eos/lhcb/testbeam/rich/tb2017Oct/data/calibration.root");
  TTree *cal = (TTree*)f->Get("calibration"); 
  Double_t thresholdstep_att0,thresholdstep_att1,thresholdstep_att2;
  Double_t offset_att0,offset_att1,offset_att2;  
  Int_t EC,PMT,CH;
  Double_t calstep[4][4][64];
  Double_t offstep[4][4][64];
  Double_t cali[4][4][64][64];
  const Int_t n = 64;
  Double_t y[64]={0.};
  Double_t xnew[64]={0.};
  Double_t x_low_new[64]={0.};
  Double_t x_high_new[64]={0.};
  Double_t ey[64]={0.};
  Double_t ex[64]={0.};
  Double_t x,x_low,x_high;

  cal->SetBranchAddress("EC",&EC);
  cal->SetBranchAddress("PMT",&PMT);
  cal->SetBranchAddress("CH",&CH);
  cal->SetBranchAddress("thresholdstep_att0",&thresholdstep_att0);
  cal->SetBranchAddress("thresholdstep_att1",&thresholdstep_att1); 
  cal->SetBranchAddress("thresholdstep_att2",&thresholdstep_att2);
  cal->SetBranchAddress("offset_att0",&offset_att0);
  cal->SetBranchAddress("offset_att1",&offset_att1);
  cal->SetBranchAddress("offset_att2",&offset_att2);
  
  for(Int_t g=0; g<cal->GetEntries(); g++){
    cal->GetEntry(g);
    Double_t   thresholdstep_att_v[3]= { thresholdstep_att0, thresholdstep_att1,thresholdstep_att2 };
    Double_t   offset_att_v[3]= {offset_att0,offset_att1, offset_att2 };
    for (int m=0; m<=2;m++) {  
      if(attenuation==m)  {
        calstep[EC][PMT][CH-1]=thresholdstep_att_v[m];  offstep[EC][PMT][CH-1]=offset_att_v[m];
      }  
    }
    for(Int_t th_step=1;th_step<=64;th_step++){
      cali[EC][PMT][CH-1][th_step]=0;
      cali[EC][PMT][CH-1][th_step]= th_step*calstep[EC][PMT][CH-1]+ offstep[EC][PMT][CH-1];

    }                                                
  }

  /////////////////////
  // Fit each pixels //
  /////////////////////

  //  TCanvas *c1 = new TCanvas("c1","multipads",900,700);
 
  TFile *output_file = new TFile( Form("fits_%s_%s_%s.root", tag.Data(), ECpos.Data(), PMTpos.Data()), "RECREATE");
   
  for(int i = 50; i < 51; i++) {

    // Get threshold scan histogram
    const int iEC = idxEC(ECpos, PMTpos); // DB numbering scheme 
    const int iPMT = idxPMT(PMTpos);
    hs[i] = (TH1F*)input_file->Get( Form("S-Curve EC%i PMT%i Ch %i", iEC, iPMT, i + 1) );
    hs[i]->SetName( Form("histogram%i", i + 1) );
    hs[i]->SetTitle( Form("histogram%i", i + 1) );
    

    //from threshold step to threshold value (ke)
    // int nbins = hs[i]->GetXaxis()->GetNbins();
    // int min_Step= hs[i]->GetXaxis()->GetBinLowEdge(min_step);
    // int max_Step= hs[i]->GetXaxis()->GetBinUpEdge(max_step);
   
//     for (int st=0;st<64;st++) { 
//       y[st]= hs[i]->GetBinContent(st);
//       ey[st]= hs[i]->GetBinError(st);
//       x = hs[i]->GetXaxis()->GetBinCenter(st);
//       x_low = hs[i]->GetXaxis()->GetBinLowEdge(st);
//       // x_high = hs[i]->GetXaxis()->GetBinUpEdge(st);// symmetric
//       xnew[st]= x*calstep[iEC][iPMT][i]+ offstep[iEC][iPMT][i];
//       x_low_new[st]= x_low*calstep[iEC][iPMT][i]+ offstep[iEC][iPMT][i];
//       //  x_high_new[st]= x_high*calstep[iEC][iPMT][i]+ offstep[iEC][iPMT][i];
//       ex[st]= xnew[st]-x_low_new[st];
//       cout<<xnew[st]<<endl;
//     }

//     h[i] = new TGraphErrors(n,xnew,y,ex,ey);
//     h[i]->SetName(Form("h%i", i + 1));
//     h[i]->SetMarkerStyle(7);
//     h[i]->Write();
//   }
   

//   h[50]->Draw("AP");
//   hs[50]->Draw("AP");
//   output_file->Write();
//   output_file->Close();
// }

    //from threshold step to threshold value (ke)
    int nbins = hs[i]->GetXaxis()->GetNbins();
    int min_Step= hs[i]->GetXaxis()->GetBinLowEdge(min_step_th);
    int max_Step= hs[i]->GetXaxis()->GetBinUpEdge(max_step_th);
 
    h[i] = new TH1F(Form("h%i", i + 1),"",nbins,  min_Step*calstep[iEC][iPMT][i]+ offstep[iEC][iPMT][i], max_Step*calstep[iEC][iPMT][i]+ offstep[EC][PMT][i]);
    
    for (int st=0;st<64;st++) {  
      h[i]->SetBinContent(st,hs[i]->GetBinContent(st));
    }

 double min_step= h[i]->GetXaxis()->GetBinLowEdge(min_step_th);
double max_step= h[i]->GetXaxis()->GetBinUpEdge(max_step_th);

  
  
//   output_file->Write();
//   output_file->Close();



  //end anna  
 


   

  //da modificare 
  // Get estimation of initial parameters
  // pedestal
  double par_pedestal = 0.;
  for(int j = min_step_th; j < max_step_th; j++) {
    if (h[i]->GetBinContent(j) > 0.8) par_pedestal = h[i]->GetXaxis()->GetBinCenter(j) ;
  }

  // gain
  TH1F* hsmooth = (TH1F*)h[i]->Clone(Form("hsmooth%i", i + 1));
  hsmooth->Smooth(1);
  TF1* hfun = new TF1(Form("hfun%i", i + 1), [&](double *x, double *p) { return hsmooth->Interpolate(x[0]); }, min_step, max_step, 0); 
  hder[i] = new TF1(Form("hder%i", i + 1), [&](double *x, double *p) { return -hfun->Derivative(x[0]); }, min_step, max_step, 0); 
  double par_gain = hder[i]->GetMaximumX(1.3*par_pedestal, max_step) - par_pedestal;


  
  // Fitting range
  const double min_fit = par_pedestal; // oppure pedestal+22%
  // const double min_fit = min_step; 
  const double max_fit = max_step;
    
  cout << endl;
  cout << "[INFO] ------------------" << endl;
  cout << "[INFO] Fitting channel " << i + 1 << endl;
  cout << "[INFO] ------------------" << endl;
  cout << "[INFO] Initial parameters:" << endl;
  cout << "pedestal: " << par_pedestal << endl;
  cout << "gain: " << par_gain << endl;
  cout << "[INFO] Fitting range: [" << min_fit << ", " << max_fit << "]" << endl;
  cout << endl;
  }
}

  /*
  //---------------- FIT SIGNAL --------------------------------------------
  // Fitting function
  totpdf[i] = new TF1(Form("totpdf%i", i + 1), tot_pdf, min_fit, max_fit, 9);
  totpdf[i]->SetLineColor(kBlue);
  gStyle->SetOptFit(1111);

  // Set fitting parameters
  totpdf[i]->SetParName(0, "mu");
  totpdf[i]->SetParameter(0, 1e-2); 
  totpdf[i]->SetParLimits(0, 0.0, 1.0);

  totpdf[i]->SetParName(1, "mu_ct");
  totpdf[i]->SetParameter(1, 1e-2); 
  totpdf[i]->SetParLimits(1, 0.0, 1.0);
  totpdf[i]->FixParameter(1, 0.); // fix

  totpdf[i]->SetParName(2, "pedestal");
  totpdf[i]->SetParameter(2, par_pedestal);
  //    totpdf[i]->FixParameter(2, par_pedestal); // fix

  totpdf[i]->SetParName(3, "sigma pedestal");
  // totpdf[i]->FixParameter(3, 1e-4); // fix
    
  totpdf[i]->SetParName(4, "gain");
  totpdf[i]->SetParameter(4, par_gain);
  totpdf[i]->SetParLimits(4, 0.7*par_gain, 1.6*par_gain);
  totpdf[i]->FixParameter(4, 16.9932*4); // fix


  totpdf[i]->SetParName(5, "sigma_sig");
  totpdf[i]->SetParameter(5, sqrt(par_gain));
  totpdf[i]->SetParLimits(5, 0., 10.*sqrt(par_gain));
  totpdf[i]->FixParameter(5, 5.569*4); // fix

  totpdf[i]->SetParName(6, "mean_ct");
  totpdf[i]->SetParameter(6, 0.05*par_gain);
  totpdf[i]->SetParLimits(6, 0.02*par_gain, 0.1*par_gain);
  totpdf[i]->FixParameter(6, 0.); // fix

  totpdf[i]->SetParName(7, "sigma_ct");
  totpdf[i]->SetParameter(7, 1.);
  totpdf[i]->SetParLimits(7, 0., 2.);
  totpdf[i]->FixParameter(7, 0.); // fix

  totpdf[i]->SetParName(8, "Pmiss");
  totpdf[i]->SetParameter(8, 0.3);
  totpdf[i]->SetParLimits(8, 0.0, 1.0);
  totpdf[i]->FixParameter(8, 0.52); // fix
  // Do fit to signal region
  h[i]->Fit(totpdf[i], "R", "G" , min_fit, max_fit);
  //------------------------------------------------------------------------

  //---------------- FIT ALL -----------------------------------------------
  //mu_fit = totpdf[i]->GetParameter("mu");
  //totpdf[i]->SetParameter(0, mu_fit); 
  //totpdf[i]->SetParLimits(0, 0.7*mu_fit, 1.3*mu_fit);

  //totpdf[i]->ReleaseParameter(1);
  //totpdf[i]->SetParameter(1, mu_fit); 
  //totpdf[i]->SetParLimits(1, 0.0, 1.5*mu_fit);
  //totpdf[i]->FixParameter(1, 0.0);

  //totpdf[i]->ReleaseParameter(2);
  //totpdf[i]->SetParameter(2, par_pedestal);
  ////totpdf[i]->SetParLimits(2, 0.98*par_pedestal, 1.02*par_pedestal);
  //totpdf[i]->FixParameter(2, par_pedestal);

  //totpdf[i]->ReleaseParameter(3);
  //totpdf[i]->SetParameter(3, 1.e-4); 
  ////totpdf[i]->SetParLimits(3, 0., 1e-3);
  //totpdf[i]->FixParameter(3, 1e-4);

  //gain_fit = totpdf[i]->GetParameter("gain");
  //totpdf[i]->SetParameter(4, gain_fit);
  //totpdf[i]->SetParLimits(4, 0.7*gain_fit, 1.3*gain_fit);
  ////totpdf[i]->FixParameter(4, gain_fit);

  //sigma_sig_fit = totpdf[i]->GetParameter("sigma_sig");
  //totpdf[i]->SetParameter(5, sigma_sig_fit);
  //totpdf[i]->SetParLimits(5, 0.7*sigma_sig_fit, 1.3*sigma_sig_fit);
  ////totpdf[i]->FixParameter(5, sigma_sig_fit);

  //totpdf[i]->ReleaseParameter(6);
  //totpdf[i]->SetParameter(6, 0.1*gain_fit);
  ////totpdf[i]->SetParLimits(6, 0.05*gain_fit, 0.2*gain_fit);
  ////totpdf[i]->FixParameter(6, 0.05*gain_fit);
  //totpdf[i]->FixParameter(6, 0.0);

  //totpdf[i]->ReleaseParameter(7);
  //totpdf[i]->SetParameter(7, 0.1*sigma_sig_fit);
  //totpdf[i]->SetParLimits(7, 0., 5.);
  //totpdf[i]->FixParameter(7, 0.0);

  //Pmiss_fit = totpdf[i]->GetParameter("Pmiss");
  //totpdf[i]->SetParameter(8, Pmiss_fit); 
  ////totpdf[i]->SetParLimits(8, 0.7*Pmiss_fit, 1.3*Pmiss_fit);
  //totpdf[i]->SetParLimits(8, 0.0, 1.0);
  ////totpdf[i]->FixParameter(8, Pmiss_fit);
 
  //// Do the full fit
  TFitResultPtr r = h[i]->Fit(totpdf[i], "R", "G" , min_fit, max_fit);
  fit_status = r;
  chi2_fit = totpdf[i]->GetChisquare() / totpdf[i]->GetNDF(); 
  histochi2->Fill(chi2_fit);
  cout << "[INFO] Fit status: " << gMinuit->fCstatu.Data() << endl;
  //------------------------------------------------------------------------

  // Get final pdfs & fit parameters
  mu_fit = totpdf[i]->GetParameter("mu");
  mu_ct_fit = totpdf[i]->GetParameter("mu_ct");
  gain_fit = totpdf[i]->GetParameter("gain");
  sigma_sig_fit = totpdf[i]->GetParameter("sigma_sig");
  ped_fit = totpdf[i]->GetParameter("pedestal");
  sigma_ped_fit = totpdf[i]->GetParameter("sigma pedestal");
  mean_ct_fit = totpdf[i]->GetParameter("mean_ct");
  sigma_ct_fit = totpdf[i]->GetParameter("sigma_ct");
  Pmiss_fit = totpdf[i]->GetParameter("Pmiss");
  gainMe_fit = gain_fit * calib[attenuation] / 1e3;

  // noise pdf
  noisepdf[i] = new TF1( Form("noisepdf%i", i + 1), 
                         [&](double *x, double *p) { return signal_crosstalk_pdf(x, 0, 0, p); }, min_step, max_step, 9 ); 
  noisepdf[i]->SetParameters(mu_fit, mu_ct_fit, ped_fit, sigma_ped_fit, gain_fit, sigma_sig_fit, mean_ct_fit, sigma_ct_fit, Pmiss_fit);
  noisepdf[i]->SetLineColor( kBlack );

  // signal pdf
  sigpdf[i] = new TF1( Form("sigpdf%i", i + 1), 
                       [&](double *x, double *p) { return signal_crosstalk_pdf(x, 1, 0, p); }, min_step, max_step, 9 ); 
  sigpdf[i]->SetParameters(mu_fit, mu_ct_fit, ped_fit, sigma_ped_fit, gain_fit, sigma_sig_fit, mean_ct_fit, sigma_ct_fit, Pmiss_fit);
  sigpdf[i]->SetLineColor( kRed );

  // cross-talk pdf
  ctpdf[i] = new TF1( Form("ctpdf%i", i + 1), 
                      [&](double *x, double *p) { return signal_crosstalk_pdf(x, 0, 1, p); }, min_step, max_step, 9 );
  ctpdf[i]->SetParameters(mu_fit, mu_ct_fit, ped_fit, sigma_ped_fit, gain_fit, sigma_sig_fit, mean_ct_fit, sigma_ct_fit, Pmiss_fit);
  ctpdf[i]->SetLineColor( kViolet );

  // Do the derivative 
  totder[i] = new TGraph(); // total
  totder[i]->SetName( Form("derivative%i", i + 1) );
  totder[i]->SetTitle( Form("derivative%i", i + 1) );
  totder[i]->SetLineColor( kBlack );

  sigder[i] = new TGraph(); // signal 
  sigder[i]->SetName( Form("derivative_sig%i", i + 1) );
  sigder[i]->SetTitle( Form("derivative_sig%i", i + 1) );
  sigder[i]->SetLineColor( kRed );

  ctder[i] = new TGraph(); // cross-talk
  ctder[i]->SetName( Form("derivative_ct%i", i + 1) );
  ctder[i]->SetTitle( Form("derivative_ct%i", i + 1) );
  ctder[i]->SetLineColor( kViolet );

  const double dx = 0.1; // fill graph w/ derivative of fit function
  double xj = min_step;
  while (xj <= max_step) {
    int j = (xj - min_step) / dx;
    totder[i]->SetPoint(j, xj, -totpdf[i]->Derivative(xj));
    sigder[i]->SetPoint(j, xj, -sigpdf[i]->Derivative(xj));
    ctder[i]->SetPoint(j, xj, -ctpdf[i]->Derivative(xj));
    xj += dx;
  }

  // Find optimized threshold based on efficiency
  th_eff = min_step;
  double sig_max = sigpdf[i]->GetMaximum();
  double sig_eff = 1.; 
  while (sig_eff >= min_efficiency && th_eff <= max_step) {
    sig_eff = sigpdf[i]->Eval(th_eff++) / sig_max;
  }

  // Signal over noise for the optimized threshold
  //double S = sigpdf[i]->Eval(th_eff);
  //double B = noisepdf[i]->Eval(th_eff);
  //double SoB = B > 0. ? S/B : -1.;

  // Find threshold without fit
  for(int j = max_step; j > min_step; j--) {
    if (h[i]->GetBinContent(j) > 0.8) {
      th_wofit = j + 5;
      break;
    }
  }

  thresholds_eff[i] = th_eff;
  thresholds_min[i] = th_min;
  thresholds_wofit[i] = th_wofit;

  // Fill histograms and tree
  histopedestal->Fill(ped_fit);
  histopeak->Fill(gain_fit);
  histomu->Fill(mu_fit);
  histosigmapedestal->Fill(sigma_ped_fit);
  histosigma->Fill(sigma_sig_fit);
  hthresholds_eff->Fill( th_eff);
  hthresholds_min->Fill( th_min);
  //hsob->Fill( SoB );
  hthresholds_wof->Fill(th_wofit);
  hder[i]->Write();


  tree->Fill();
} //ipx

  ///////////////////////////////
  // Write thresholds XML file // 
  ///////////////////////////////
writeXML( "95_eff_" + descr, ECpos, PMTpos, thresholds_eff, offset, attenuation );
writeXML( "min_" + descr, ECpos, PMTpos, thresholds_min, offset, attenuation );
writeXML( "wofit_" + descr, ECpos, PMTpos, thresholds_wofit, offset, attenuation );


////////////////
// Save plots //
////////////////
TCanvas *chist = new TCanvas("chist", "S-curve", 1000, 1000);
chist->Divide(8, 8);

TCanvas *cder = new TCanvas("cder", "S-curve derivative", 1000, 1000);
cder->Divide(8, 8);

for (int i = 0; i < NPX; i++) { 
  TVirtualPad* pad = chist->cd(i + 1); 
  pad->SetLogy();
  h[i]->Draw(); 
  noisepdf[i]->Draw("same");
  sigpdf[i]->Draw("same");

  pad = cder->cd(i + 1); 
  pad->SetLogy();
  totder[i]->Draw(); 
 }

for (int i = 0; i < NPX; i++) {
  h[i]->Write();
  totpdf[i]->Write();
  noisepdf[i]->Write();
  sigpdf[i]->Write();
  ctpdf[i]->Write();
  totder[i]->Write();
  sigder[i]->Write();
  ctder[i]->Write();
 }

histochi2->Write();
histopedestal->Write();
histosigmapedestal->Write();
histopeak->Write();
histomu->Write();
histosigma->Write();
hthresholds_eff->Write();
hthresholds_min->Write();
hthresholds_wof->Write();
hsob->Write();
chist->Write();
cder->Write();
output_file->Write();
output_file->Close();
//chist->SaveAs( Form("fits_%s_EC%i_PMT%i.pdf", tag.Data(), iEC, iPMT) );
//cder->SaveAs( Form("derivatives_%s_EC%i_PMT%i.pdf", tag.Data(), iEC, iPMT) );
  
}
*/
#endif
