#include <stdio.h>
#include <iomanip>
#include <fstream>
#include <vector>
#include <iostream>

#include "Fit.h"
#include <TApplication.h>

using namespace std;

// EC & PMT positions on cold-bar
// TT A C
//    B D
// MT A C
//    B D
// MB A C
//    B D
// BB A C
//    B D

int main(int argc, char** argv) {
  // External arguments
  if (argc < 6) { 
    cout << "[USAGE] " << argv[0] << " input_file th_min th_max offset attenuation [xml_name]" << endl;
    exit(0);
  }
  const TString filename = argv[1]; // input file
  const int min_step = atoi(argv[2]); // th. min
  const int max_step = atoi(argv[3]); // th. max
  const int offset = atoi(argv[4]);  // offset
  const int attenuation = atoi(argv[5]); // attenuation
  const TString descr = argv[6]; // run description

  //TApplication theApp("App",&argc,argv);

  // Fit threshold scans
  for (TString ECpos : {"TT", "MT", "MB", "BB"}) {
    for (TString PMTpos : {"A", "B", "C", "D"}) {
      if (ECpos != "MT" || PMTpos != "A") continue;
      //if (ECpos != "MB" || PMTpos != "A") continue;
      fit(filename, min_step, max_step, offset, attenuation, ECpos, PMTpos, descr);
    }
  }

  //  theApp.Run();
  
  return 0;
}



