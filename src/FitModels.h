#ifndef FITMODELS_H
#define FITMODELS_H

#include <stdio.h>
#include <iomanip>
#include <fstream>
#include <iostream>

#include <TMath.h>
#include <TROOT.h>

using namespace std;


/////////////////////////////
// Signal + cross-talk PDF //
/////////////////////////////
double signal_crosstalk_pdf(double *x, int nph, int nct, double *par) {
  // Max. number of missing stages
  const int Nmissed = 6;

  // Param. for modelling conversion on dynodes
  // Gi = G**1/N * (ri**k / (r1*r2)**k/N) = G**1/N * G0
  const int  Ndynodes = 12;
  const double r1 = 2.3;
  const double r2 = 1.2;
  const double k = 2./3.;
  const double pcorr = 0.3;
  const double corr_fact = 0.45; // corr. factor wrt dynode conversion model

  // Floating params.
  double mu_pe = par[0]; // mu signal
  double mu_ct = par[1]; // mu cross-talk
  double ped = par[2];   // pedestal pos
  double sigma_ped = par[3]; // pedestal width
  double gain = par[4]; // gain
  double sigma_sig = par[5]; // signal width
  double mean_ct = par[6]; //0.03*gain;  // cross-talk position
  double sigma_ct = par[7]; //0.9*sqrt(mean_ct); // cross-talk width
  double Pmiss = par[8]; // probability to loss gain

  double pdf = 0.;
 
  // Noise (step function)
  //if (nph == 0 && nct == 0) pdf = (ped > x[0]) ? 1. : 0.;
  
  // One photon plus cross-talk
  if (nph == 1) {
    double G0 = 1.;
    // Add gain loss at each stage
    for (int nmiss = 0; nmiss <= Nmissed; nmiss++) {
      if (nmiss == 1) G0 *= corr_fact * pow(r1, k) / pow(r1*r2, k/Ndynodes);
      if (nmiss == 2) G0 *= corr_fact * pow(r2, k) / pow(r1*r2, k/Ndynodes);
      if (nmiss > 2)  G0 *= corr_fact / pow(r1*r2, k/Ndynodes);
      double gmiss = G0 * pow(gain*1e3, double(nmiss) / Ndynodes);

      double mean = ped + nct*mean_ct + gain/gmiss;
      double sigma = sqrt(sigma_ped*sigma_ped + sigma_sig*sigma_sig/gmiss + nct*sigma_ct*sigma_ct);
      double coeff = 0.;
      if (nmiss == 0) coeff = 1. - Pmiss;
      else if (nmiss == 1) coeff = Pmiss / (1. + (Nmissed -1)*pcorr);
      else coeff = pcorr * Pmiss / (1. + (Nmissed -1)*pcorr);
      pdf += coeff * 0.5 * (1 - TMath::Erf((x[0] - mean) / sigma / TMath::Sqrt(2.))); 
    }
  }

  // All other cases (ignore convolution...)
  else {
    double mean = ped + nct*mean_ct + nph*gain;
    double sigma = sqrt(sigma_ped*sigma_ped + nph*sigma_sig*sigma_sig + nct*sigma_ct*sigma_ct);
    pdf = 0.5 * (1 - TMath::Erf((x[0] - mean) / sigma / TMath::Sqrt(2.))); 
  }

  // normalize by the Poisson factor
  double norm = TMath::Poisson(nph, mu_pe) * TMath::Poisson(nct, mu_ct);
  pdf *= norm;

  return pdf;
}


///////////////
// Total PDF //
///////////////
double tot_pdf(double *x, double *par) {
  // Max. number of photons
  const int Nphotons = 5;
  
  // Max. number of cross-talk events
  const int Ncrosstalks = 1;
  
  // Make total pdf 
  double totpdf = 0.;
  for (int nph = 0; nph <= Nphotons; nph++) {
    for (int nct = 0; nct <= Ncrosstalks; nct++) {
      totpdf += signal_crosstalk_pdf(x, nph, nct, par);
    }
  }

  return totpdf;
}
#endif



