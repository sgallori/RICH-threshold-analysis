// EC & PMT positions on cold-bar
// TT A C
//    B D
// MT A C
//    B D
// MB A C
//    B D
// BB A C
//    B D
void plotColdBar(const int runNumber) {
  // PMT dimension 
  const int dimPMT = 8;

  // ECs
  const std::list<TString> ECs = {"TT", "MT", "MB", "BB"};

  // 2inch ECs
  const std::list<TString> ECHs = {"MB", "BB"};

  // PMTs
  const std::list<TString> PMTs = {"A", "B", "C", "D"};


  // Set ROOT batch mode
  gROOT->SetBatch();

  // Tells if EC is 2inch type`
  auto is2inch = [&](const TString &EC) { 
    return (std::find(ECHs.begin(), ECHs.end(), EC) != ECHs.end()); 
  };

  // Returns EC dimension
  auto dimEC = [&](const TString &EC) { return (is2inch(EC) ? 1 : 2); };

  // Returns PMT pixel of the pair (row, col)
  auto pmtPixel = [&](const TString &PMT, const int i, const int j) { 
    const int N = dimPMT;
    int ipx;
    if (PMT == "A") ipx = N*(N -1 -i) + (N -1 -j);
    else if (PMT == "B") ipx = N*(N -1 -j) + i;
    else if (PMT == "D") ipx = N*i + j;
    else if (PMT == "C") ipx = N*j + N -1 -i;
    else ipx = -1;
    return ipx + 1;
  };

  // Returns EC pad
  auto ECPad = [] (const TString &EC) { 
    int pad;
    if (EC == "TT") pad = 1;
    else if (EC == "MT") pad = 2;
    else if (EC == "MB") pad = 3;
    else if (EC == "BB") pad = 4;
    else pad = -1;
    return pad;
  };

  // Returns PMT pad
  auto PMTPad = [] (const TString &PMT) { 
    int pad;
    if (PMT == "A") pad = 1;
    else if (PMT == "B") pad = 3;
    else if (PMT == "C") pad = 2;
    else if (PMT == "D") pad = 4;
    else pad = -1;
    return pad;
  };


  auto canvColdBar = new TCanvas("canvColdBar", "Cold bar", 800, 1500);
  canvColdBar->Divide(1, ECs.size()); // create coldbar

  // Loop over ECs
  for (const auto &EC : ECs) {
    std::cout << "[INFO] Drawing EC " << EC << "..." << std::endl;

    // create PMTs in EC
    auto canvEC = canvColdBar->cd( ECPad(EC) );
    canvEC->Divide(dimEC(EC), dimEC(EC)); 

    // Loop over PMTs
    for (const auto &PMT : PMTs) {
      if (is2inch(EC) && PMT != "A") continue; // 2inch PMTs is only "A"

      // Open file
      TString fileName = Form("fits_histo%i_%s_%s.root", runNumber, EC.Data(), PMT.Data());
      if (gSystem->AccessPathName(fileName)) {
	std::cout << "[WARNING] File for " << EC << " not found! Skip!" << std::endl;
	continue;
      }
      auto file = TFile::Open(fileName, "READ");

      // create PMT pixels
      int ipad = 0;
      auto canvPMT = canvEC->cd( PMTPad(PMT) );
      canvPMT->Divide(dimPMT, dimPMT); 

      // Loop over row, col of PMT
      for (int i = 0; i < dimPMT; i++) { 
	for (int j = 0; j < dimPMT; j++) { 
	  int ipx =  pmtPixel(PMT, i, j);
	  auto h = (TH1F*)file->Get( Form("h%i", ipx) ); 
	  auto sigder = (TGraph*)file->Get( Form("derivative_sig%i", ipx) ); 

	  canvPMT->cd(++ipad); 
	  canvPMT->SetLogy(); 
	  sigder->GetYaxis()->SetRangeUser(1e-4, 1.e-3); 
	  sigder->Draw(); 
	  //h->GetYaxis()->SetRangeUser(0.05, 1); 
	  //h->Draw();
	}//j
      }//i
    }//PMT
  }//EC

  // Save canvas
  std::cout << "[INFO] Saving canvas..." << std::endl;
  canvColdBar->SaveAs( "Coldbar.png" );
}
