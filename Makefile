# Exec name
EXEC := fit

# Paths
SRC := src

# Compiler
CXX = g++

# Compiler flags
ROOTCONFIG := root-config
ROOTCFLAGS := $(shell $(ROOTCONFIG) --cflags)
CXXFLAGS := -Wall -fPIC -O3 -std=c++11 # C++11
CXXFLAGS += $(ROOTCFLAGS)

# Extra libs
ROOTLIBS  := $(shell $(ROOTCONFIG) --libs) -lMinuit 
EXTRALIBS := $(PERSLIBS) $(ROOTLIBS)

# Src, obj, inc
SRCS = $(wildcard $(SRC)/*.cpp)
INCS = $(wildcard $(SRC)/*.h)
OBJS = $(SRCS:.cpp=.o)

.PHONY: all clean

all: $(EXEC)

# Make exec
$(EXEC): $(OBJS)  
	$(CXX) $(CXXFLAGS) $(EXTRALIBS) $^ -o $@

# Make objs
%.o: %.cpp $(INCS)
	$(CXX) $(CXXFLAGS) $(EXTRALIBS) -c $< -o $@ 

# Clean
clean: 
	rm $(SRC)/*.o
	rm $(EXEC)
